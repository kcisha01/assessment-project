
#pragma once
#include "BST.h"

	//AVL stands for Adelson, Velski & Landis 
    //It is a self balancing binary search tree
	
	class AVL : public BST
	{
	public:
		bool displayRotations = true;

		//works out height of sub tree
		int height(Number* node);

		//difference between left and right sub trees
		int difference(Number* node);

		//Rotations
		//return: new parent of subtree
		//parameter: current parent of sub tree
		//right branch, right child
		Number* RRrotation(Number* parent);
		//left branch, left child
		Number* LLrotation(Number* parent);
		//left branch, right child
		Number* LRrotation(Number* parent);
		//right branch, left child
		Number* RLrotation(Number* parent);

		
		//balances a tree structure where parent is the middle top node
		//returns new parent after balancing(rotations)
		Number* balance(Number* parent);

		
		//recursive insert that considers parent a sub tree
		//this insert also balances itself
		//returns the new root node of the tree
		Number* insertAVL(Number* parent, Number* newStudent);

		//overriding insert from parent
		void insert(Number* newNumber);
	};




