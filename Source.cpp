#include<iostream>
#include<fstream>
#include<cstdlib>
#include"BST.h"       //include the classes used
#include "AVL.h"
using namespace std;
int main()
{
	//create an input file and store the numbers
	ofstream fout ;
	fout.open("input-a1q2.txt", ios::out);

	fout << "5\n" << "30\t38\t21\t16\t12\t";
	
	fout.close();
	
	int ch;

	//create object of classes BST and AVL
	BST bst1;
	AVL avl1;

	//read from "input-a1q1.txt" and then 
	//write to the output file "output-a1q1.txt"
	ifstream fin;
	ofstream fileoutput;
	fin.open("input-a1q2.txt",ios::in); 
	fileoutput.open("output-a1q2.txt");
	fin >> ch;     //read the amount of numbers to be added to tree
	
	//read all the numbers and then add to the tree
	//store the values in output file

	fin >> ch;           
	avl1.insert(new Number(ch));
	fileoutput << ch << "\t";

	fin >> ch;
	avl1.insert(new Number(ch));
	fileoutput << ch << "\t";

	fin >> ch;
	avl1.insert(new Number(ch));
	fileoutput << ch << "\t";

	fin >> ch;
	avl1.insert(new Number(ch));
	fileoutput << ch << "\t";

	fin >> ch;
	avl1.insert(new Number(ch));	
	fileoutput << ch << "\t";

	//print the level of the AVL Tree along with its values
	avl1.show(bst1.root);

	fin.close();
	return 0;  //end

}