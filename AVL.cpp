#include "AVL.h"


//determine the height of the AVL Tree
int AVL::height(Number* node)
{
    int h = 0;
    //helps break recursion cycle when we get to nulls at the bottom of branches
    if (node != NULL)
    {
        int leftH = height(node->leftChild);
        int rightH = height(node->rightChild);

        //maxH stores the maximum height out of two different heights
        int maxH = max(leftH, rightH);
        h = maxH + 1;
    }
    return h;
}

//calculate the balance factor
//if it is -1,0,1 then tree is balanced 
//else it is not
int AVL::difference(Number* node)
{
    //if empty treee, well its balanced, its 0
    if (node == NULL)
        return 0;

    int leftH = height(node->leftChild);
    int rightH = height(node->rightChild);
    int balanceFactor = leftH - rightH;

    return balanceFactor;
}

//if the tree in unbalanced and right-right rotation needs to be performed
//then execute this function

Number* AVL::RRrotation(Number* parent)
{
    Number* temp = parent->rightChild;
    parent->rightChild = temp->leftChild;
    temp->leftChild = parent;
    if (displayRotations)

    return temp;
}

//if the tree in unbalanced and left-left rotation needs to be performed
//then execute this function

Number* AVL::LLrotation(Number* parent)
{
    Number* temp = parent->leftChild;
    parent->leftChild = temp->rightChild;
    temp->rightChild = parent;
    if (displayRotations)

    return temp;
}

//if the tree in unbalanced and left-right rotation needs to be performed
//then execute this function
//first rotation rotates bottom 2 nodes which turns the whole structure into a RR rotation
//second rotation uses RRrotation

Number* AVL::LRrotation(Number* parent)
{
    Number* temp = parent->leftChild;
    parent->leftChild = RRrotation(temp);
    if (displayRotations)
    return LLrotation(parent);
}

//if the tree in unbalanced and right-left rotation needs to be performed
//then execute this function
//first rotation rotates bottom 2 nodes which turns the whole structure into a LL rotation
//second rotation uses LLrotation

Number* AVL::RLrotation(Number* parent)
{
    Number* temp = parent->rightChild;
    parent->rightChild = LLrotation(temp);
    if (displayRotations)
    return RRrotation(parent);
}

//get the balnce factor 
//if balancing is needed then execute the required balancing function
Number* AVL::balance(Number* parent)
{
    
    int balanceFactor = difference(parent);

    if (balanceFactor > 1)
    {
        //left branch is heavy, now work out is left or right child heavy
        if (difference(parent->leftChild) > 0)
        {
            //left child unbalanced
            parent = LLrotation(parent);
        }
        else
        {
            //right child unbalanced
            parent = LRrotation(parent);
        }
    }
    else if (balanceFactor < -1)
    {
        //right branch is heavy, but which child
        if (difference(parent->rightChild) > 0)
        {
            //left child heavy
            parent = RLrotation(parent);
        }
        else
        {
            //right child heavy
            parent = RRrotation(parent);
        }
    }


    return parent;
}

//insert into the AVL tree
Number* AVL::insertAVL(Number* parent, Number* newNumber)
{
    //if sub tree empty, this becomes the parent
    if (parent == NULL)
    {
        parent = newNumber;
        return parent;
    }

    //parent not null, so we haven't found an empty space to stick new student yet
    //so we need to go down either left or right path
    if (newNumber->numbercode < parent->numbercode)
    {
        parent->leftChild = insertAVL(parent->leftChild, newNumber);
        parent = balance(parent);
    }
    else //assume id >= parent's id
    {
        parent->rightChild = insertAVL(parent->rightChild, newNumber);
        parent = balance(parent);
    }
}

//overridden function over BST's insert function
void AVL::insert(Number* newNumber)
{
    root = insertAVL(root, newNumber);
    
    cout << endl;
}
