#pragma once
#include <queue>
#include "Number.h"

//This is a binary search tree
//where smaller value is placed to the left and bigger to right of the tree

class BST
{
public:
	Number* root = NULL;
	virtual void insert(Number* current);
	//recursive traversal functions
	void show(Number* p);
	
};



//class which will help in displaying level 
class NumberLevelNode {
public:
	Number* numbercode;
	int level;

	//constructor
	NumberLevelNode(Number*numbercode, int level)
	{
		this->numbercode =numbercode;
		this->level = level;
	}
};


