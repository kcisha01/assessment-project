#include "BST.h"

using namespace std;
void BST::insert(Number* newNumber)
{

    // When the root is empty,tree is empty 
    //So,add to the root and end the loop
    if (root == NULL)
    {
        root = newNumber;
        return; 
    }

   //the parent of root is always null 
    //assuming we are first in the root node through 'current'
    
    Number* parent = NULL; 
    Number* current = root;
    while (true)//infinite loop
    {
        
        parent = current;
        
        //binary search strategy 
        //i.e. if value is smaller than root fo to left else right
        if (newNumber->numbercode < current->numbercode)
        {
            
            current = current->leftChild;

            //if empty space is found , then insert our value to that space
            if (current == NULL)
            {
                
                parent->leftChild = newNumber;
                return;      //can end; we have already inserted the value
            }
        }
        else
        {
            //go down right path
            current = current->rightChild;
            //if current is NULL, insert there
            if (current == NULL)
            {
                parent->rightChild = newNumber;
                return;
            }
        }
    }

}




/*https://www.geeksforgeeks.org/level-order-tree-traversal/*/
void BST::show(Number* p)
{
    // Base Case 
    if (root == NULL)  return ;

    // Create an empty queue for level order traversal 
    queue<NumberLevelNode> q;

    // Enqueue Root and initialize height 
    q.push(NumberLevelNode(root, 0));

    int previousOutputLevel = -1;

    while (q.empty() == false)
    {
        // Print front of queue and remove it from queue 
        //the level along with the value gets printed
        NumberLevelNode node = q.front();
        if (node.level != previousOutputLevel)
        {
            cout << endl;
            cout << node.level << "- ";
            previousOutputLevel = node.level;
        }
        

        cout << node.numbercode->numbercode<<"\t";
        q.pop();

        /* Enqueue left child */
        if (node.numbercode->leftChild != NULL)
            q.push(NumberLevelNode(node.numbercode->leftChild, node.level + 1));

        /*Enqueue right child */
        if (node.numbercode->rightChild != NULL)
            q.push(NumberLevelNode(node.numbercode->rightChild, node.level + 1));
    }
    
}








